var express = require('express');
var app = express();

app.set('view engine','jade');
app.set('view options', {layout: true});
app.set('views', __dirname + '/views');

app.get('/hello/:name', function(req, res, next){
	var name = req.params.name;
	res.render('index', {'name': name});
});

app.listen(8888);